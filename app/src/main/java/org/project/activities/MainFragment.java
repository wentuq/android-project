package org.project.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.project.R;
import org.project.Utils;
import org.project.fragments.Fragment2;
import org.project.fragments.GPSLocation;
import org.project.fragments.SectionsStatePagerAdapter;
import org.project.fragments.UserInfo;
import org.project.users.DbUser;
import org.project.users.User;

public class MainFragment extends AppCompatActivity {

    private SectionsStatePagerAdapter mSectionStatePagerAdapter;
    private ViewPager mViewPager;
    Button mfbtnLogout;
    TextView tvUser;
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;

    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);
        Log.i(Utils.tag, "onCreate: Started.");
        sharedPrefs = Utils.getSharedPrefs(this);
        editor = Utils.getSharedPrefsEditor(this, sharedPrefs);

        user =  logUserId();

        tvUser = (TextView) findViewById(R.id.mf_tv_user);
        mfbtnLogout = (Button) findViewById(R.id.mf_btnLogout);

        tvUser.setText(user.getUsername());
        mfbtnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLogoutClicked();
            }
        });


        mSectionStatePagerAdapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.containter);
        setupViewPager(mViewPager);



    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsStatePagerAdapter adapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new GPSLocation(), "GPSLocation");
        adapter.addFragment(new Fragment2(), "Fragment2");
        adapter.addFragment(new UserInfo(), "UserInfo");

        viewPager.setAdapter(adapter);
        // set first visible fragment
        //viewPager.setCurrentItem(1);
    }


    public void setViewPager(int fragmentNumber) {
        mViewPager.setCurrentItem(fragmentNumber);
    }



    private void onLogoutClicked() {
        Utils.logOut(editor);
        Intent intent = new Intent(this, ActivityLogin.class);
        startActivity(intent);
    }


    public User logUserId() {
        DbUser dbUser = new DbUser(this);
        int dbUserId = sharedPrefs.getInt("dbUserId", -1);
        User user = dbUser.getUser(dbUserId);
        return user;

    }

    public User getUserForFragments() {
        return user;
    }
}
