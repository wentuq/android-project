package org.project.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.project.R;
import org.project.Utils;
import org.project.users.DbUser;
import org.project.users.User;

import java.text.ParseException;
import java.util.Calendar;

public class ActivitySignup extends AppCompatActivity {

    Button sgnbtnBackLogin;
    Button sgnbtnSignIn;
    EditText sgnetUsername;
    EditText sgnetPassword;
    RadioGroup sgnrgRadio;
    RadioButton sgnRadioButton;
    RadioButton sgnlastRadioButton;
    AutoCompleteTextView sgntvAutocomplete;
    Button sgnbtnBirth;
    CheckBox sgnchkTerms;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    String username;
    String password;
    String sex;
    String country;
    Calendar cal;
    int day, month, year;
    String birth;
    boolean terms;
    int radioId = -1;
    int dbUserId = -1;

    DbUser dbUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        cal = setCalendarVars(Calendar.getInstance());
        dbUser = new DbUser(this);

        assignView();
        getSharedPrefs();
        setListeners();
        textChangedListeners();
        setCountryAdapter();


    }

    private void assignView() {
        sgnbtnBackLogin = (Button) findViewById(R.id.sgnbtnBackLogin);
        sgnbtnSignIn = (Button) findViewById(R.id.sgnbtnSignIn);
        sgnetUsername = (EditText) findViewById(R.id.sgnetUsername);
        sgnetPassword = (EditText) findViewById(R.id.sgnetPassword);
        sgnrgRadio = (RadioGroup) findViewById(R.id.sgnrgRadio);
        sgntvAutocomplete = (AutoCompleteTextView) findViewById(R.id.sgntvAutocomplete);
        sgnbtnBirth = (Button) findViewById(R.id.sgnbtnBirth);
        sgnchkTerms = (CheckBox) findViewById(R.id.sgnchkTerms);
        sgnlastRadioButton = (RadioButton) findViewById(R.id.sgnradio3);


    }

    private void getSharedPrefs() {
        sharedPref = Utils.getSharedPrefs(this);
        editor = Utils.getSharedPrefsEditor(this,sharedPref);
        sgnetUsername.setText(sharedPref.getString("sgnusername", ""));
//        sgnetPassword.setText(sharedPref.getString("sgnpassword", ""));
        radioId = sharedPref.getInt("sgnsex", -1);
        sgnrgRadio.check(radioId);
        sgntvAutocomplete.setText(sharedPref.getString("sgncountry", ""));
        sgnbtnBirth.setText(sharedPref.getString("sgnbirth", "Select"));
        sgnchkTerms.setChecked(sharedPref.getBoolean("sgnterms", false));

    }

    private void resetSharedPref() {
        editor.clear();
        editor.commit();
    }


    private void textChangedListeners() {
        sgnetUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validateUsername();

            }
        });

        sgntvAutocomplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validateCountry();

            }
        });
    }

    private void setListeners() {

        sgnbtnBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackLoginClicked();
            }
        });

        sgnbtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    onSignInClicked();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });


        sgnrgRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                radioId = i;
                Button b = (Button) findViewById(R.id.sgnradio3);
                editor.putInt("sgnsex", radioId);
                editor.commit();
                b.setError(null);
            }
        });


        sgnbtnBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });

        sgnchkTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    sgnchkTerms.setError(null);
                    editor.putBoolean("sgnterms", true);
                    editor.commit();
                } else {
                    editor.putBoolean("sgnterms", false);
                    editor.commit();
                    sgnchkTerms.setError("You have to agree!");
                }
            }
        });

    }

    private String getSex() {
        String sex = "";
        if (radioId != -1) {
            sgnRadioButton = findViewById(radioId);
            sex = sgnRadioButton.getText().toString().trim();
        }
        Log.i(Utils.tag, "Radio Button Selected: " + radioId);
        return sex;

    }

    public void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(ActivitySignup.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int yearF, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String date = dayOfMonth + "/" + monthOfYear + "/" + yearF;
                sgnbtnBirth.setText(date);
                year = yearF;
                month = monthOfYear;
                day = dayOfMonth;

                editor.putString("sgnbirth", date);
                editor.commit();
                sgnbtnBirth.setError(null);

            }
        }, year, month - 1, day);
        datePickerDialog.show();
    }

    private String getUsername() {
        String username = "";
        username = sgnetUsername.getText().toString().trim();
        return username;
    }

    private String getPassword() {
        String password = "";
        password = sgnetPassword.getText().toString().trim();
        return password;
    }


    private void setCountryAdapter() {
        Resources res = getResources();

        String[] countries = res.getStringArray(R.array.countries_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, countries);
        sgntvAutocomplete.setAdapter(adapter);

    }

    private String getCountry() {
        String country = "";
        country = sgntvAutocomplete.getText().toString().trim();
        return country;
    }

    private String getBirth() {
        String birth = sgnbtnBirth.getText().toString().trim();
        if (birth.equals("Select")) birth = "";
        return birth;
    }


    public Calendar setCalendarVars(Calendar cal) {
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH) + 1;
        year = cal.get(Calendar.YEAR);

        return cal;
    }

    private boolean getTerms() {
        Log.i(Utils.tag, "checkTerms : " + sgnchkTerms.isChecked());
        return sgnchkTerms.isChecked();
    }


    private void onBackLoginClicked() {
        Log.d(Utils.tag, "onBackLoginClicked");

        Intent intent = new Intent(this, ActivityLogin.class);
        startActivity(intent);

    }

    private void onSignInClicked() throws ParseException {
        Log.d(Utils.tag, "onSignInClicked");
        if (validate()) {
            User user = setUser();
            dbUserId = dbUser.insertNewUser(user);
            editor.putInt("dbUserId", dbUserId);
            Utils.clearSignInPrefs(editor);
            editor.commit();
            Intent intent = new Intent(this, MainFragment.class);

            startActivity(intent);
        } else {
            Log.i(Utils.tag, "Not validated!");
        }

    }


    private User setUser() {
        User user = new User();
        user.setUsername(getUsername());
        user.setPassword(getPassword());
        user.setCountry(getCountry());
        user.setBirth(getBirth());
        user.setSex(getSex());
        return user;
    }

    private boolean validate() {
        boolean validate = true;
        birth = getBirth();
        terms = getTerms();
        sex = getSex();


        if (!validateUsername()) validate = false;
        if (!validatePassword()) validate = false;
        if (!validateCountry()) validate = false;


        if (radioId == -1) {
            sgnlastRadioButton.setError("Choose sex");
            validate = false;
        }
        if (birth.isEmpty()) {
            sgnbtnBirth.setError("Choose!");
            return false;
        }
        if (terms == false) {
            sgnchkTerms.setError("You have to agree!");
            return false;
        }

        return validate;


    }

    private boolean validateUsername() {
        boolean validate = true;
        String username = getUsername();
        editor.putString("sgnusername", username);
        editor.commit();
        if (username.length() < 3) {
            sgnetUsername.setError("Enter more than 2 characters");
            validate = false;
        }

        return validate;
    }

    private boolean validatePassword() {
        boolean validate = true;
        String password = getPassword();
//        editor.putString("sgnpassword", password);
//        editor.commit();
        if (password.length() < 3) {
            sgnetPassword.setError("Enter more than 2 characters");
            validate = false;
        }
        return validate;
    }

    private boolean validateCountry() {
        boolean validate = true;
        String country = getCountry();
        editor.putString("sgncountry", country);

        if (country.isEmpty()) {
            sgntvAutocomplete.setError("Cannot be blank");
            validate = false;
        }
        return validate;
    }


}
