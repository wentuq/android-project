package org.project.activities;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class Permission {
    public static final int REQUEST_WRITE_STORAGE = 112;
    public static final int REQUEST_ACCESS_LOCATION = 100;
    public static final int REQUEST_ACCESS_INTERNET = 101;
    public static final int REQUEST_ACCESS_INTERNET_LOCATION = 202;

    public static boolean isWriteGranted(Context ctx) {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    public static boolean isGPSGranted(Context ctx) {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }
    public static boolean isInternetGranted(Context ctx) {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(ctx, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }
    public static boolean isGPSandInternetGranted(Context ctx) {
        if(isGPSGranted(ctx) && isInternetGranted(ctx)) return true;
        return false;
    }

    public static void requestWrite(Activity activity) {
        if (!isWriteGranted(activity)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
    }

    public static void requestGPS(Activity activity) {
        if (!isGPSGranted(activity)) {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ACCESS_LOCATION);
        }
    }


    public static void requestInternet(Activity activity) {
        if (!isInternetGranted(activity)) {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.INTERNET}, REQUEST_ACCESS_INTERNET);
        }
    }

    public static void requestGPSInternet(Activity activity) {
        if(!isGPSandInternetGranted(activity)) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.INTERNET}, REQUEST_ACCESS_INTERNET_LOCATION);
        }
    }



}
