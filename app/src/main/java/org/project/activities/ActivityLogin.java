package org.project.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.project.R;
import org.project.Utils;
import org.project.users.DbUser;
import org.project.users.User;

public class ActivityLogin extends AppCompatActivity {


    RelativeLayout rellay1;
    RelativeLayout rellay2;
    Button btnLogin;
    Button btnSignUp;
    EditText etUsername;
    EditText etPassword;

    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;

    DbUser dbUser;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            rellay1.setVisibility(View.VISIBLE);
            rellay2.setVisibility(View.VISIBLE);
        }
    };

    int dbUserId = -1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler.postDelayed(runnable, 2000);
        checkIfLoggedIn();
        setContentView(R.layout.activity_login);
        dbUser = new DbUser(this);

        rellay1 = (RelativeLayout) findViewById(R.id.rellay1);
        rellay2 = (RelativeLayout) findViewById(R.id.rellay2);
        btnLogin = (Button) findViewById(R.id.btnLogIn);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);

        etUsername = (EditText)  findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);

        sharedPrefs = Utils.getSharedPrefs(this);
        editor = Utils.getSharedPrefsEditor(this,sharedPrefs);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLoginClicked();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSignUpClicked();
            }
        });


                runJingle();
    }

    void checkIfLoggedIn() {
        sharedPrefs = Utils.getSharedPrefs(this);
        dbUserId = sharedPrefs.getInt("dbUserId", -1);
        if(dbUserId!=-1) {
            Log.i(Utils.tag, "User logged in before UserId: " + dbUserId);
            Intent intent = new Intent(this,MainFragment.class);
            startActivity(intent);
        }
    }

    void runJingle() {
        MediaPlayer mp = MediaPlayer.create(getBaseContext(), R.raw.wait_a_second);
        mp.start();
    }

    private void onSignUpClicked() {
        Log.d(Utils.tag, "onSignUpClicked");

        Intent intent = new Intent("android.intent.action.ActivitySignup");
        startActivity(intent);

    }
    private void onLoginClicked() {
        Log.d(Utils.tag, "onLoginClicked");


        User user = dbUser.getUser(etUsername.getText().toString(),etPassword.getText().toString());
        resetInput();
        if(!user.getUsername().isEmpty()) {
            Log.i(Utils.tag, "Login UserId: " + user.getUserId());
            Log.i(Utils.tag, "Correctly Logged in");
            editor.putInt("dbUserId", user.getUserId());
            Utils.clearSignInPrefs(editor);
            editor.commit();
            Intent intent = new Intent(this,MainFragment.class);
            startActivity(intent);

        }
        else {
            Log.i(Utils.tag, "Incorrect username or password");
            Toast.makeText(this,"Incorrect username or password", Toast.LENGTH_SHORT).show();
        }

    }


    private void resetInput() {
        etUsername.setText("");
        etPassword.setText("");
    }


}
