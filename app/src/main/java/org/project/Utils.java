package org.project;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utils {
    public static final String tag = "TAG";
    public static final String CHANNEL_ID ="gpsServiceChannel";

    public static boolean isMyServiceRunning(Context ctx, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static SharedPreferences getSharedPrefs(Activity ctx) {
        return ctx.getSharedPreferences("GENERAL", Context.MODE_PRIVATE);
    }

    public static SharedPreferences getPrefs(Activity ctx) {
        return ctx.getPreferences(Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getSharedPrefsEditor(Activity ctx) {
        SharedPreferences.Editor editor;
        return getSharedPrefs(ctx).edit();
    }

    public static SharedPreferences.Editor getSharedPrefsEditor(Activity ctx, SharedPreferences prefs) {
        return prefs.edit();
    }

    public static void clearSignInPrefs(SharedPreferences.Editor editor) {
        editor.remove("sgnusername");
        editor.remove("sgnpassword");
        editor.remove("sgnsex");
        editor.remove("sgncountry");
        editor.remove("sgnbirth");
        editor.remove("sgnterms");
        editor.commit();
    }

    public static void logOut(SharedPreferences.Editor editor) {
        editor.remove("dbUserId");
        editor.commit();
    }


    public static long getTimeMillis(String dateString, String dateFormat) throws ParseException {
        /*Use date format as according to your need! Ex. - yyyy/MM/dd HH:mm:ss */
        String myDate = dateString;//"2017/12/20 18:10:45";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat/*"yyyy/MM/dd HH:mm:ss"*/);
        Date date = sdf.parse(myDate);
        long millis = date.getTime();

        return millis;
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat/*"yyyy/MM/dd HH:mm:ss"*/);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static long getCurrentDate() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH)+1;
        int year = calendar.get(Calendar.YEAR);
        return getTimeMillis(day+"/"+month+"/" +year,"dd/MM/yyyy");

    }


}

