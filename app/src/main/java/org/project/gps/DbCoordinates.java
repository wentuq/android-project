package org.project.gps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.project.users.User;

public class DbCoordinates extends SQLiteOpenHelper {
    private static final String DB_NAME="DBDevCoords";
    private static final int DB_VER=1;
    public static final String DB_TABLE="Coordinates";
    public static final String DB_COLUMN_LAT ="Lat";
    public static final String DB_COLUMN_LON="Lon";
    public static final String DB_COLUMN_DATE="Date";
    public static final String DB_COLUMN_ID="_id";
    public static final String DB_COLUMN_USER_ID="user_id";

    public DbCoordinates(Context ctx) {
        super(ctx, DB_NAME,null,DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s REAL NOT NULL, %s REAL NOT NULL, %s DATE NOT NULL, %s INTEGER NOT NULL);",
                DB_TABLE,DB_COLUMN_ID, DB_COLUMN_LAT, DB_COLUMN_LON, DB_COLUMN_DATE, DB_COLUMN_USER_ID);
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String query = String.format("DELETE TABLE IF EXISTS %s", DB_TABLE);
        db.execSQL(query);
        onCreate(db);
    }

    public int insertCoords(Coordinates c, int user_id)  {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DB_COLUMN_LAT,c.getLat());
        values.put(DB_COLUMN_LON,c.getLon());
        values.put(DB_COLUMN_DATE,c.getTime());
        values.put(DB_COLUMN_USER_ID, user_id);
        int coordsId = (int) db.insertWithOnConflict(DB_TABLE,null,values,SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
        return coordsId;
    }


    public Coordinates getCoord(int cID) {
        SQLiteDatabase db = this.getWritableDatabase();
        Coordinates c = new Coordinates();
        Cursor cursor = db.rawQuery("SELECT * FROM Coordinates WHERE ID=?", new String[]{DB_TABLE,String.valueOf(cID)});
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            c = setCoordsDB(cursor);
        }
        cursor.close();
        db.close();
        return c;

    }

    public Cursor getCoordsCursor(int coordsID) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM Coordinates WHERE ID=?", new String[]{String.valueOf(coordsID)});
        return cursor;


    }
    public Cursor getCoordsCursor() {
        SQLiteDatabase db = this.getWritableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM Coordinates",null);
        return cursor;

    }
    public Cursor getCoordsLimitCursor(int num, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM Coordinates where user_id = ? ORDER BY _id DESC LIMIT ?",new String[]{String.valueOf(userId), String.valueOf(num)});
        return cursor;
    }

    public static Coordinates setCoordsDB(Cursor cursor) {
        Coordinates c = new Coordinates();
        if(cursor !=null) {
            c.setLat(cursor.getDouble(cursor.getColumnIndex(DB_COLUMN_LAT)));
            c.setLon(cursor.getDouble(cursor.getColumnIndex(DB_COLUMN_LON)));
            c.setTime(cursor.getLong(cursor.getColumnIndex(DB_COLUMN_DATE)));
        }
        return c;
    }

    public void deleteCoord(int cId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DB_TABLE, DB_COLUMN_ID + " = ?",new String[]{String.valueOf(cId)});
//        db.delete(DB_TABLE,null,null);
        db.close();
    }
    public void deleteDB(int cId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DB_TABLE,null,null);
        db.close();
    }

}
