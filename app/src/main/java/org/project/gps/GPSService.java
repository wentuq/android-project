package org.project.gps;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.project.R;
import org.project.Utils;
import org.project.activities.MainFragment;

public class GPSService extends Service {

    private LocationListener locationListener;
    private LocationManager locationManager;
    private DbCoordinates dbCoordinates;
    private int minTime;
    private int minDistance;
    private SharedPreferences sharedPreferences;
    private int dbUserId;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        dbUserId =
        this.minDistance = intent.getIntExtra("minDistance", 5000);
        this.minTime = intent.getIntExtra("minTime", 1);
        this.dbUserId = intent.getIntExtra("dbUserId", -1);


        Intent notificationIntent = new Intent(this, MainFragment.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, Utils.CHANNEL_ID)
                .setContentTitle("GPS Service")
                .setContentText("GPS location is running")
                .setSmallIcon(R.drawable.black_logo)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);


        dbCoordinates = new DbCoordinates(getApplicationContext());
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Intent i = new Intent("location_update");
                Log.i(Utils.tag, "Coord are: " + location.getLatitude() + "," + location.getLongitude() + " time is: " + GPX.miliToTimeString(location.getTime()));
                i.putExtra("coordinates", location.getLatitude() + "," + location.getLongitude());
                sendBroadcast(i);
                Coordinates c = new Coordinates(location.getLatitude(), location.getLongitude(), location.getTime());

                dbCoordinates.insertCoords(c, dbUserId);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        };

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        Log.i(Utils.tag, "minTime:" + minTime + "minDistance: " + minDistance);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, locationListener);


        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(Utils.tag, "onDestroy GPSService");
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
    }
}
