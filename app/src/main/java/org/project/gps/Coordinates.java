package org.project.gps;

import android.location.Location;

public class Coordinates {
    private Double lat;
    private Double lon;
    private long time;


    public Coordinates(Double lat, Double lon, long time) {
        this.setLat(lat);
        this.setLon(lon);
        this.setTime(time);
    }

    public Coordinates(Location l) {
        this.setLat(l.getLatitude());
        this.setLon(l.getLongitude());
        this.setTime(l.getTime());
    }

    public Coordinates() {
        this.lat = 0.0;
        this.lon = 0.0;
        this.time = 0;
    }

    public Double getLat() {

        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getCoordinates() {
        String lat = String.format("%.4f", getLat());
        String lon = String.format("%.4f", getLon());

        return "" + lat + "," + lon;

    }


}
