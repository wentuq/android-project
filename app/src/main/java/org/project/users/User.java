package org.project.users;

public class User {
    private String username;
    private String password;
    private String sex;
    private String country;
    private String birth;
    private int userId;

    public User(String username, String password, String sex, String country, String birth) {
        this.username = username;
        this.password = password;
        this.sex = sex;
        this.country = country;
        this.birth = birth;
        this.userId = -1;
    }



    public User() {
        this.username = "";
        this.password = "";
        this.sex = "";
        this.country = "";
        this.birth = "";
        this.userId = -1;

    }
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }
}
