package org.project.users;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.project.gps.DbCoordinates;
import org.project.users.User;
import org.project.Utils;

import java.text.ParseException;

public class DbUser extends SQLiteOpenHelper {
    private static final String DB_NAME="DBDev";
    private static final int DB_VER=1;
    public static final String DB_TABLE="User";
    public static final String DB_COLUMN_USERNAME ="Username";
    public static final String DB_COLUMN_PASSWORD="Password";
    public static final String DB_COLUMN_SEX="Sex";
    public static final String DB_COLUMN_COUNTRY="Country";
    public static final String DB_COLUMN_BIRTH="Birth";
    public static final String DB_COLUMN_ID="ID";

    public DbUser(Context ctx) {
        super(ctx, DB_NAME,null,DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s DATE NOT NULL);",
                DB_TABLE,DB_COLUMN_ID, DB_COLUMN_USERNAME,DB_COLUMN_PASSWORD, DB_COLUMN_SEX, DB_COLUMN_COUNTRY, DB_COLUMN_BIRTH);
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String query = String.format("DELETE TABLE IF EXISTS %s", DB_TABLE);
        db.execSQL(query);
        onCreate(db);
    }

    public int insertNewUser(User user) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DB_COLUMN_USERNAME,user.getUsername());
        values.put(DB_COLUMN_PASSWORD,user.getPassword());
        values.put(DB_COLUMN_SEX,user.getSex());
        values.put(DB_COLUMN_COUNTRY,user.getCountry());
        values.put(DB_COLUMN_BIRTH, Utils.getTimeMillis(user.getBirth(),"dd/MM/yyyy"));
        int userId = (int) db.insertWithOnConflict(DB_TABLE,null,values,SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
        return userId;
    }

    public User getUser(String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM User WHERE Username=?", new String[]{username});

        Log.i(Utils.tag,"" + cursor.getCount() + " " +  cursor.getColumnCount());
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            if(checkUsername(cursor,username) && checkPassword(cursor,password)) {
                Log.i(Utils.tag, "DB getUser by username and password correctly");
                user = setUserFromDB(cursor);
            }
        }
        cursor.close();
        db.close();
        return user;
    }

    private boolean checkPassword(Cursor c, String password) {
        String password_db = c.getString(c.getColumnIndex(DB_COLUMN_PASSWORD));
        return password.equals(password_db);
    }
    private boolean checkUsername(Cursor c, String username) {
        String username_db = c.getString(c.getColumnIndex(DB_COLUMN_USERNAME));
        return username.equals(username_db);
    }

    public User getUser(int userID) {
        SQLiteDatabase db = this.getWritableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM User WHERE ID=?", new String[]{String.valueOf(userID)});
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            user = setUserFromDB(cursor);
        }
        cursor.close();
        db.close();
        return user;

    }

    public Cursor getUserCursor(int userID) {
        SQLiteDatabase db = this.getWritableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM User WHERE ID=?", new String[]{String.valueOf(userID)});
        db.close();
        return cursor;


    }
    public Cursor getUsersCursor() {
        SQLiteDatabase db = this.getWritableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM User",null);
        db.close();
        return cursor;

    }

    public User setUserFromDB(Cursor c) {
        User user = new User();
        user.setUsername(c.getString(c.getColumnIndex(DB_COLUMN_USERNAME)));
        user.setPassword(c.getString(c.getColumnIndex(DB_COLUMN_PASSWORD)));
        user.setSex(c.getString(c.getColumnIndex(DB_COLUMN_SEX)));
        user.setCountry(c.getString(c.getColumnIndex(DB_COLUMN_COUNTRY)));
        user.setBirth(Utils.getDate(c.getLong(c.getColumnIndex(DB_COLUMN_BIRTH)),"dd/MM/yyyy"));
        user.setUserId(c.getInt(c.getColumnIndex(DB_COLUMN_ID)));
        Log.i(Utils.tag, "getUserId: " + user.getUserId());
        return user;

    }

    public void deleteUser(int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DB_TABLE, DB_COLUMN_ID + " = ?",new String[]{String.valueOf(userId)});
//        db.delete(DB_TABLE,null,null);
        db.close();
    }

}
