package org.project;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForecastService {
    private static final String APP_URL="https://api.darksky.net/";
    private static final String APP_KEY="4c64aaca1402af54a4a3e3e02fb9b8eb";
    private double lat;
    private double lon;

    public ForecastService(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public void loadForecastData(Callback<Forecast> forecast) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ForecastAPI service = retrofit.create(ForecastAPI.class);
        Call<Forecast> call = service.getForecast(APP_KEY, String.valueOf(lat), String.valueOf(lon));
        call.enqueue(forecast);
    }
}
