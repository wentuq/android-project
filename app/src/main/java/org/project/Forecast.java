package org.project;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Forecast {


    private double latitude;
    private double longitude;
    private String timezone;
    private int offset;
    private CurrentlyBean currently;
    private HourlyBean hourly;
    private DailyBean daily;
    private FlagsBean flags;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public CurrentlyBean getCurrently() {
        return currently;
    }

    public void setCurrently(CurrentlyBean currently) {
        this.currently = currently;
    }

    public HourlyBean getHourly() {
        return hourly;
    }

    public void setHourly(HourlyBean hourly) {
        this.hourly = hourly;
    }

    public DailyBean getDaily() {
        return daily;
    }

    public void setDaily(DailyBean daily) {
        this.daily = daily;
    }

    public FlagsBean getFlags() {
        return flags;
    }

    public void setFlags(FlagsBean flags) {
        this.flags = flags;
    }

    public static class CurrentlyBean {
        private int time;
        private String summary;
        private String icon;
        private double precipIntensity;
        private double precipProbability;
        private double temperature;
        private double apparentTemperature;
        private double dewPoint;
        private double humidity;
        private double windSpeed;
        private int windBearing;
        private double visibility;
        private double cloudCover;
        private double pressure;
        private double ozone;

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public double getPrecipIntensity() {
            return precipIntensity;
        }

        public void setPrecipIntensity(double precipIntensity) {
            this.precipIntensity = precipIntensity;
        }

        public double getPrecipProbability() {
            return precipProbability;
        }

        public void setPrecipProbability(double precipProbability) {
            this.precipProbability = precipProbability;
        }

        public double getTemperature() {
            return temperature;
        }

        public void setTemperature(double temperature) {
            this.temperature = temperature;
        }

        public double getApparentTemperature() {
            return apparentTemperature;
        }

        public void setApparentTemperature(double apparentTemperature) {
            this.apparentTemperature = apparentTemperature;
        }

        public double getDewPoint() {
            return dewPoint;
        }

        public void setDewPoint(double dewPoint) {
            this.dewPoint = dewPoint;
        }

        public double getHumidity() {
            return humidity;
        }

        public void setHumidity(double humidity) {
            this.humidity = humidity;
        }

        public double getWindSpeed() {
            return windSpeed;
        }

        public void setWindSpeed(double windSpeed) {
            this.windSpeed = windSpeed;
        }

        public int getWindBearing() {
            return windBearing;
        }

        public void setWindBearing(int windBearing) {
            this.windBearing = windBearing;
        }

        public double getVisibility() {
            return visibility;
        }

        public void setVisibility(double visibility) {
            this.visibility = visibility;
        }

        public double getCloudCover() {
            return cloudCover;
        }

        public void setCloudCover(double cloudCover) {
            this.cloudCover = cloudCover;
        }

        public double getPressure() {
            return pressure;
        }

        public void setPressure(double pressure) {
            this.pressure = pressure;
        }

        public double getOzone() {
            return ozone;
        }

        public void setOzone(double ozone) {
            this.ozone = ozone;
        }
    }

    public static class HourlyBean {
        private String summary;
        private String icon;
        private List<DataBean> data;

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            private int time;
            private String summary;
            private String icon;
            private double precipIntensity;
            private double precipProbability;
            private double temperature;
            private double apparentTemperature;
            private double dewPoint;
            private double humidity;
            private double windSpeed;
            private int windBearing;
            private double visibility;
            private double cloudCover;
            private double pressure;
            private double ozone;
            private String precipType;

            public int getTime() {
                return time;
            }

            public void setTime(int time) {
                this.time = time;
            }

            public String getSummary() {
                return summary;
            }

            public void setSummary(String summary) {
                this.summary = summary;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public double getPrecipIntensity() {
                return precipIntensity;
            }

            public void setPrecipIntensity(double precipIntensity) {
                this.precipIntensity = precipIntensity;
            }

            public double getPrecipProbability() {
                return precipProbability;
            }

            public void setPrecipProbability(double precipProbability) {
                this.precipProbability = precipProbability;
            }

            public double getTemperature() {
                return temperature;
            }

            public void setTemperature(double temperature) {
                this.temperature = temperature;
            }

            public double getApparentTemperature() {
                return apparentTemperature;
            }

            public void setApparentTemperature(double apparentTemperature) {
                this.apparentTemperature = apparentTemperature;
            }

            public double getDewPoint() {
                return dewPoint;
            }

            public void setDewPoint(double dewPoint) {
                this.dewPoint = dewPoint;
            }

            public double getHumidity() {
                return humidity;
            }

            public void setHumidity(double humidity) {
                this.humidity = humidity;
            }

            public double getWindSpeed() {
                return windSpeed;
            }

            public void setWindSpeed(double windSpeed) {
                this.windSpeed = windSpeed;
            }

            public int getWindBearing() {
                return windBearing;
            }

            public void setWindBearing(int windBearing) {
                this.windBearing = windBearing;
            }

            public double getVisibility() {
                return visibility;
            }

            public void setVisibility(double visibility) {
                this.visibility = visibility;
            }

            public double getCloudCover() {
                return cloudCover;
            }

            public void setCloudCover(double cloudCover) {
                this.cloudCover = cloudCover;
            }

            public double getPressure() {
                return pressure;
            }

            public void setPressure(double pressure) {
                this.pressure = pressure;
            }

            public double getOzone() {
                return ozone;
            }

            public void setOzone(double ozone) {
                this.ozone = ozone;
            }

            public String getPrecipType() {
                return precipType;
            }

            public void setPrecipType(String precipType) {
                this.precipType = precipType;
            }
        }
    }

    public static class DailyBean {
        private String summary;
        private String icon;
        private List<DataBeanX> data;

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public List<DataBeanX> getData() {
            return data;
        }

        public void setData(List<DataBeanX> data) {
            this.data = data;
        }

        public static class DataBeanX {
            private int time;
            private String summary;
            private String icon;
            private int sunriseTime;
            private int sunsetTime;
            private double moonPhase;
            private double precipIntensity;
            private double precipIntensityMax;
            private double precipProbability;
            private double temperatureMin;
            private int temperatureMinTime;
            private double temperatureMax;
            private int temperatureMaxTime;
            private double apparentTemperatureMin;
            private int apparentTemperatureMinTime;
            private double apparentTemperatureMax;
            private int apparentTemperatureMaxTime;
            private double dewPoint;
            private double humidity;
            private double windSpeed;
            private int windBearing;
            private double visibility;
            private double cloudCover;
            private double pressure;
            private double ozone;
            private int precipIntensityMaxTime;
            private String precipType;

            public int getTime() {
                return time;
            }

            public void setTime(int time) {
                this.time = time;
            }

            public String getSummary() {
                return summary;
            }

            public void setSummary(String summary) {
                this.summary = summary;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public int getSunriseTime() {
                return sunriseTime;
            }

            public void setSunriseTime(int sunriseTime) {
                this.sunriseTime = sunriseTime;
            }

            public int getSunsetTime() {
                return sunsetTime;
            }

            public void setSunsetTime(int sunsetTime) {
                this.sunsetTime = sunsetTime;
            }

            public double getMoonPhase() {
                return moonPhase;
            }

            public void setMoonPhase(double moonPhase) {
                this.moonPhase = moonPhase;
            }

            public double getPrecipIntensity() {
                return precipIntensity;
            }

            public void setPrecipIntensity(int precipIntensity) {
                this.precipIntensity = precipIntensity;
            }

            public double getPrecipIntensityMax() {
                return precipIntensityMax;
            }

            public void setPrecipIntensityMax(int precipIntensityMax) {
                this.precipIntensityMax = precipIntensityMax;
            }

            public double getPrecipProbability() {
                return precipProbability;
            }

            public void setPrecipProbability(double precipProbability) {
                this.precipProbability = precipProbability;
            }

            public double getTemperatureMin() {
                return temperatureMin;
            }

            public void setTemperatureMin(double temperatureMin) {
                this.temperatureMin = temperatureMin;
            }

            public int getTemperatureMinTime() {
                return temperatureMinTime;
            }

            public void setTemperatureMinTime(int temperatureMinTime) {
                this.temperatureMinTime = temperatureMinTime;
            }

            public double getTemperatureMax() {
                return temperatureMax;
            }

            public void setTemperatureMax(double temperatureMax) {
                this.temperatureMax = temperatureMax;
            }

            public int getTemperatureMaxTime() {
                return temperatureMaxTime;
            }

            public void setTemperatureMaxTime(int temperatureMaxTime) {
                this.temperatureMaxTime = temperatureMaxTime;
            }

            public double getApparentTemperatureMin() {
                return apparentTemperatureMin;
            }

            public void setApparentTemperatureMin(double apparentTemperatureMin) {
                this.apparentTemperatureMin = apparentTemperatureMin;
            }

            public int getApparentTemperatureMinTime() {
                return apparentTemperatureMinTime;
            }

            public void setApparentTemperatureMinTime(int apparentTemperatureMinTime) {
                this.apparentTemperatureMinTime = apparentTemperatureMinTime;
            }

            public double getApparentTemperatureMax() {
                return apparentTemperatureMax;
            }

            public void setApparentTemperatureMax(double apparentTemperatureMax) {
                this.apparentTemperatureMax = apparentTemperatureMax;
            }

            public int getApparentTemperatureMaxTime() {
                return apparentTemperatureMaxTime;
            }

            public void setApparentTemperatureMaxTime(int apparentTemperatureMaxTime) {
                this.apparentTemperatureMaxTime = apparentTemperatureMaxTime;
            }

            public double getDewPoint() {
                return dewPoint;
            }

            public void setDewPoint(double dewPoint) {
                this.dewPoint = dewPoint;
            }

            public double getHumidity() {
                return humidity;
            }

            public void setHumidity(double humidity) {
                this.humidity = humidity;
            }

            public double getWindSpeed() {
                return windSpeed;
            }

            public void setWindSpeed(double windSpeed) {
                this.windSpeed = windSpeed;
            }

            public int getWindBearing() {
                return windBearing;
            }

            public void setWindBearing(int windBearing) {
                this.windBearing = windBearing;
            }

            public double getVisibility() {
                return visibility;
            }

            public void setVisibility(double visibility) {
                this.visibility = visibility;
            }

            public double getCloudCover() {
                return cloudCover;
            }

            public void setCloudCover(double cloudCover) {
                this.cloudCover = cloudCover;
            }

            public double getPressure() {
                return pressure;
            }

            public void setPressure(double pressure) {
                this.pressure = pressure;
            }

            public double getOzone() {
                return ozone;
            }

            public void setOzone(double ozone) {
                this.ozone = ozone;
            }

            public int getPrecipIntensityMaxTime() {
                return precipIntensityMaxTime;
            }

            public void setPrecipIntensityMaxTime(int precipIntensityMaxTime) {
                this.precipIntensityMaxTime = precipIntensityMaxTime;
            }

            public String getPrecipType() {
                return precipType;
            }

            public void setPrecipType(String precipType) {
                this.precipType = precipType;
            }
        }
    }

    public static class FlagsBean {

        private String units;
        private List<String> sources;
        @SerializedName("isd-stations")
        private List<String> isdstations;
        @SerializedName("madis-stations")
        private List<String> madisstations;

        public String getUnits() {
            return units;
        }

        public void setUnits(String units) {
            this.units = units;
        }

        public List<String> getSources() {
            return sources;
        }

        public void setSources(List<String> sources) {
            this.sources = sources;
        }

        public List<String> getIsdstations() {
            return isdstations;
        }

        public void setIsdstations(List<String> isdstations) {
            this.isdstations = isdstations;
        }

        public List<String> getMadisstations() {
            return madisstations;
        }

        public void setMadisstations(List<String> madisstations) {
            this.madisstations = madisstations;
        }
    }
}
