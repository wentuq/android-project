package org.project;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class Storage {

    private Context context;
    public String DIR;
    public File STORAGE_EXTERNAL;

    public Storage(Context ctx) throws IOException {
        context = ctx;
        STORAGE_EXTERNAL = getPrivateAlbumStorageDir();
    }


    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public String writeToFile(String file, String data) throws IOException {

        String ext = ".gpx";
        File ff = new File(STORAGE_EXTERNAL, file + ext);

        for (int num = 0; ff.exists(); num++) {
            ff = new File(STORAGE_EXTERNAL, file + num + ext);
        }
        FileUtils.writeStringToFile(ff, data, "UTF-8");
        return ff.getName();

    }

    public String readFromFile(String file) throws IOException {
        File f = new File(STORAGE_EXTERNAL,file);
        String s = "";
        if(f.isFile()) {
            s = FileUtils.readFileToString(f, "UTF-8");
        }
        return s;
    }


    public File getPrivateAlbumStorageDir() throws IOException {
        File file_dir = null;
        if(isExternalStorageWritable()) {
            String albumName = "gpx_track";
            file_dir = new File(context.getExternalFilesDir(null), albumName);
            if(!file_dir.exists() && !file_dir.isDirectory()) {
                if (!file_dir.mkdirs()) {
                    Log.e("TAG", "Directory not created");
                }
            }
            STORAGE_EXTERNAL = file_dir;
            DIR = file_dir.getAbsolutePath();
            Log.i(Utils.tag,"Write storage: " + DIR);

        }
        return file_dir;
    }
}
