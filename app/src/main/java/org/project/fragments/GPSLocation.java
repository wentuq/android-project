package org.project.fragments;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.project.Storage;
import org.project.activities.MainFragment;
import org.project.activities.Permission;
import org.project.gps.Coordinates;
import org.project.gps.DbCoordinates;
import org.project.gps.GPSService;
import org.project.gps.GPStracker;
import org.project.R;
import org.project.Utils;
import org.project.gps.GPX;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.internal.Util;

public class GPSLocation extends Fragment {


    private Button gpsbtnServiceStart;
    private Button btnSaveGPX;
    private TextView gpsTextView;
    private BroadcastReceiver broadcastReceiver;

    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor editor;

    private View view;
    private DbCoordinates dbCoords;
    private ListView gpsListView;
    private CoordsCursorAdapter coordsCursorAdapter;
    private ImageButton btnGpsSettings;
    private View alertView;
    private int minDistance =5;
    private int minTime =2000;
    private int fetchedElements = 50;
    private int savedElements = 100;
    private EditText etfetchedElements;
    private EditText etsavedElements;
    private SeekBar seekBarMinDistance;
    private SeekBar seekBarMinTIime;
    private EditText tvminDistance;
    private EditText tvminTime;
    private ImageButton btnRefresh;
    private int userId;


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_gpslocation_layout, container, false);

        sharedPrefs = Utils.getSharedPrefs(getActivity());
        editor = Utils.getSharedPrefsEditor(getActivity(), sharedPrefs);
        userId = ((MainFragment)getActivity()).getUserForFragments().getUserId();
        Log.i(Utils.tag, "onCreateView: started");
        initGPSView();
        setGpsButton();
        setSaveGPXButton();
        setGPSCursorAdapter();
        setGpsSettings(inflater);
        setRefreshButton();
        createNotificationChannel();


        return view;
    }

    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    Utils.CHANNEL_ID,
                    "GPS Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getActivity().getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }

    }

    public void setRefreshButton() {
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setGPSCursorAdapter();
            }
        });
    }

    public void setGpsSettings(final LayoutInflater inflater) {
        btnGpsSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertView = inflater.inflate(R.layout.alert_dialog_gps, null,false);
                AlertDialog dialog = new AlertDialog.Builder(getContext())
                        .setTitle("Set GPS settings")
                        .setMessage("Please set minimum distance and time")
                        .setView(alertView)
                        .setPositiveButton("Set", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                minDistance = Integer.valueOf(tvminDistance.getText().toString());
                                minTime = Integer.valueOf(tvminTime.getText().toString())*1000;
                                fetchedElements = Integer.valueOf(etfetchedElements.getText().toString());
                                savedElements = Integer.valueOf(etsavedElements.getText().toString());
                            }
                        })
                        .setNegativeButton("Cancel",null)
                        .create();

                tvminDistance = alertView.findViewById(R.id.alert_tvminDistance);
                tvminTime =  alertView.findViewById(R.id.alert_tvminTime);
                seekBarMinDistance = alertView.findViewById(R.id.alert_seekbar_minDistance);
                seekBarMinTIime = alertView.findViewById(R.id.alert_seekbar_minTime);
                etfetchedElements = alertView.findViewById(R.id.alert_et_fetchedElements);
                etsavedElements = alertView.findViewById(R.id.alert_et_savedElements);
                seekBarMinDistance.setProgress(minDistance);
                seekBarMinTIime.setProgress(minTime/1000);
                tvminDistance.setText(String.valueOf(minDistance));
                tvminTime.setText(String.valueOf(minTime/1000));
                etfetchedElements.setText(String.valueOf(fetchedElements));
                etsavedElements.setText(String.valueOf(savedElements));
                seekBarMinTIime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        tvminTime.setText(String.valueOf(progress));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                seekBarMinDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        tvminDistance.setText(String.valueOf(progress));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                dialog.show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.i(Utils.tag, "broadcastReceiver.onReceive Getting new coords now!");
                    gpsTextView.setText(intent.getExtras().getString("coordinates"));

                }
            };
        }
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("location_update"));
    }

    @Override
    public void onDestroy() {
        Log.i(Utils.tag, "onDestroy called");
        super.onDestroy();
        if (broadcastReceiver != null) {
            getActivity().unregisterReceiver(broadcastReceiver);
        }
    }


    public void showAlertDialog() {

    }

    private void setGpsButton() {
        if (!Permission.isGPSGranted(getActivity())) {
            gpsbtnServiceStart.setText("Get GPS Permissions");
        }
        else if(Utils.isMyServiceRunning(getContext(),GPSService.class)) {
            gpsbtnServiceStart.setText("Stop Service");
        }
        else {
            gpsbtnServiceStart.setText("Start Service");
        }

        gpsbtnServiceStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(Utils.tag,"minDistance: " + minDistance + " minTime: " + minTime);
                String text = gpsbtnServiceStart.getText().toString();
                if (Permission.isGPSGranted(getContext())) {
                    if (!Utils.isMyServiceRunning(getContext(),GPSService.class)) {
                        startServiceGPS(minTime,minDistance);
                        gpsbtnServiceStart.setText("Stop Service");
                    } else {
                        stopServiceGPS();
                        gpsbtnServiceStart.setText("Start Service");
                    }
                } else {
                    Permission.requestGPS(getActivity());
                }
            }
        });
    }




    private void setSaveGPXButton() {
        if (!Permission.isWriteGranted(getContext())) {
            btnSaveGPX.setText(("Get Write SD Permission"));
        }
        else {
            btnSaveGPX.setText(("Write GPX"));
        }
        btnSaveGPX.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String text = btnSaveGPX.getText().toString();
                Log.i(Utils.tag, "onClick Works");
                if(Permission.isWriteGranted(getContext())) {
                    Log.i(Utils.tag, "Permission granted");
                    if (text.equals("Write GPX")) {
                        Log.i(Utils.tag, "Equals Write GPX");
                        writeToStorage();
                    }
                    else {
                        Log.i(Utils.tag, "Else");
                        btnSaveGPX.setText("Write GPX");
                    }
                }
                else {
                    Log.i(Utils.tag, "Else Else");
                    Permission.requestWrite(getActivity());
                }
            }
        });
    }





    private void initGPSView() {
        gpsbtnServiceStart = (Button) view.findViewById(R.id.btnServiceStart);
        gpsTextView = (TextView) view.findViewById(R.id.gps_coords);
        gpsListView = (ListView) view.findViewById(R.id.gps_listview);
        btnSaveGPX = (Button) view.findViewById(R.id.btnSaveGPX);
        btnGpsSettings = (ImageButton) view.findViewById(R.id.btnGpsSettings);
        btnRefresh = (ImageButton) view.findViewById(R.id.gps_btnrefresh);
    }

    private void setGPSCursorAdapter() {
        Log.i(Utils.tag, "Fetching fetchedElements=" + fetchedElements);
        dbCoords = new DbCoordinates(getActivity());
        Cursor mCursor = dbCoords.getCoordsLimitCursor(fetchedElements, userId);
        coordsCursorAdapter = new CoordsCursorAdapter(getActivity(),mCursor);
        gpsListView.setAdapter(coordsCursorAdapter);
        coordsCursorAdapter.notifyDataSetChanged();
    }



    private ArrayList<Coordinates> createCoordinatesList(Cursor mCursor) {
        ArrayList<Coordinates> mArrayList = new ArrayList<Coordinates>();
        mCursor.moveToFirst();
        while(!mCursor.isAfterLast()) {
            mArrayList.add(DbCoordinates.setCoordsDB(mCursor)); //add the item
            mCursor.moveToNext();
        }
        return mArrayList;
    }

    private void writeToStorage() {
        try {
            Log.i(Utils.tag,"write to Storage");
            Storage s = new Storage(getActivity());
            DbCoordinates dbCoordinates = new DbCoordinates(getActivity());
            Log.i(Utils.tag, "Saving savedElements=" + savedElements);
            Cursor c = dbCoordinates.getCoordsLimitCursor(savedElements, userId);
            ArrayList<Coordinates> coordinates = createCoordinatesList(c);
            String content = GPX.create("unknown", coordinates);
            String filename = s.writeToFile("track", content);
            Toast.makeText(getActivity(), "Saved as: " + filename,Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void stopServiceGPS() {
        Intent i = new Intent(getContext(), GPSService.class);
        getActivity().stopService(i);
    }

    private void startServiceGPS(int minTime, int minDistance) {
        Intent i = new Intent(getContext(), GPSService.class);
        i.putExtra("minTime", minTime);
        i.putExtra("minDistance", minDistance);
        i.putExtra("dbUserId", userId);
        getActivity().startService(i);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Permission.REQUEST_ACCESS_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if(!Utils.isMyServiceRunning(getContext(),GPSService.class)) {
                    gpsbtnServiceStart.setText("Start Service");
                }
                else {
                    gpsbtnServiceStart.setText("Stop Service");
                }

            }
            else {
                gpsbtnServiceStart.setText("Get GPS Permissions");
            }
        }
        if (requestCode == Permission.REQUEST_WRITE_STORAGE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                btnSaveGPX.setText("Write GPX");
            }
            else {
                btnSaveGPX.setText("Get Write SD Permission");
            }
        }
    }


}
