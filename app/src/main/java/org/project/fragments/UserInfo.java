package org.project.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.project.R;
import org.project.activities.MainFragment;
import org.project.users.User;
import org.project.Utils;

public class UserInfo extends Fragment {
    private TextView uiUsername;
    private TextView uiUserId;
    private TextView uiSex;
    private TextView uiCountry;
    private TextView uiBirth;
    User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_userinfo_layout, container, false);
        user = new User();
        user = ((MainFragment)getActivity()).getUserForFragments();

        uiUserId = (TextView) view.findViewById(R.id.ui_userid);
        uiUsername = (TextView) view.findViewById(R.id.ui_username);
        uiSex = (TextView) view.findViewById(R.id.ui_sex);
        uiCountry = (TextView) view.findViewById(R.id.ui_country);
        uiBirth = (TextView) view.findViewById(R.id.ui_birth);


        uiUserId.setText(String.valueOf(user.getUserId()));
        uiUsername.setText(user.getUsername());
        uiSex.setText(user.getSex());
        uiCountry.setText(user.getCountry());
        uiBirth.setText(user.getBirth());



        Log.i(Utils.tag, "onCreateView: started");

        return view;
    }
}
