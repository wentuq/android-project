package org.project.fragments;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.project.Forecast;
import org.project.ForecastService;
import org.project.R;
import org.project.Utils;
import org.project.activities.MainFragment;
import org.project.activities.Permission;
import org.project.gps.Coordinates;
import org.project.gps.GPStracker;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment2 extends Fragment {
    private ForecastService forecastService;
    private Button btnGetLocation;
    private LinearLayout linlay11;
    private TextView tvlastLocation;
    private Button btnGetInfo;
    private TextView tvtempSummary;
    private TextView tvtemp;
    private TextView tvlocation;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.frag_main_layout, container, false);
        btnGetLocation = (Button) view.findViewById(R.id.frag_main_btnGetLocation);
        linlay11 = (LinearLayout) view.findViewById(R.id.frag_main_linlay11);
        tvlastLocation = (TextView) view.findViewById(R.id.frag_main_lastLocation);
        btnGetInfo = (Button) view.findViewById(R.id.frag_main_btnGetInfo);
        tvtempSummary = (TextView) view.findViewById(R.id.frag_main_tempsummary);
        tvtemp = (TextView) view.findViewById(R.id.frag_main_temp);
        tvlocation = (TextView) view.findViewById(R.id.frag_main_location);


        setGPSButton();
        setTempButton();

        return view;
    }




    private void setTempButton() {
        if (!Permission.isGPSandInternetGranted(getContext())) {
            btnGetInfo.setText(("Get Permissions"));
        }
        else {
            btnGetInfo.setText(("Get Info"));
        }
        btnGetInfo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String text = btnGetInfo.getText().toString();
                if(Permission.isGPSandInternetGranted(getContext())) {
                    if (text.equals("Get Info")) {
                        Coordinates c = getGPStrackerLocation();
                        String s = getAddressString(c);
                        tvlocation.setText(s);
                        getForecast(c);
                    }
                    else {
                        Log.i(Utils.tag, "Else");
                        btnGetInfo.setText("Get Info");
                    }
                }
                else {
                    Log.i(Utils.tag, "Else Else");
                    Permission.requestGPSInternet(getActivity());
                }
            }
        });
    }

    private void setGPSButton() {
        if (!Permission.isGPSGranted(getContext())) {
            btnGetLocation.setText(("Get Permissions"));
        }
        else {
            btnGetLocation.setText(("Get location"));
        }
        btnGetLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String text = btnGetLocation.getText().toString();
                if(Permission.isGPSGranted(getContext())) {
                    if (text.equals("Get location")) {
                        linlay11.setVisibility(View.VISIBLE);
                        Coordinates c = getGPStrackerLocation();
                        tvlastLocation.setText("" + c.getLat() + "," + c.getLon());
                    }
                    else {
                        linlay11.setVisibility(View.VISIBLE);
                        Coordinates c = getGPStrackerLocation();
                        tvlastLocation.setText("" + c.getLat() + "," + c.getLon());
                    }
                }
                else {
                    Log.i(Utils.tag, "Else Else");
                    Permission.requestGPS(getActivity());
                }
            }
        });
    }


    private Address getAddress(Coordinates c) {
        Geocoder geocoder = new Geocoder(getContext());
        ArrayList<Address> addresses;
        Address address = new Address(getActivity().getResources().getConfiguration().locale);
        try {
            addresses = (ArrayList<Address>) geocoder.getFromLocation(c.getLat(),c.getLon(),1);
            address = addresses.get(0);
            Log.i(Utils.tag, address.getLocality() + "," + address.getCountryName());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    private String getAddressString(Coordinates c) {
        Address a = getAddress(c);
        String s = a.getLocality() + ", " + a.getCountryName();
        return s;
    }



    private Coordinates getGPStrackerLocation() {
        Coordinates c = new Coordinates();
        GPStracker g = new GPStracker(getContext());
        Location l = g.getLocation();
        if (l != null) {
            c = new Coordinates(l);
        }
        return c;
    }

    private void getForecast(Coordinates c) {
        forecastService = new ForecastService(c.getLat(), c.getLon());
        Callback<Forecast> forecast = new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response) {
                Double temp = response.body().getCurrently().getTemperature();
                String summary = response.body().getCurrently().getSummary();
                tvtempSummary.setText( summary);
                tvtemp.setText(String.valueOf(temp));
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {
                tvtemp.setText("" + t.getMessage());
                tvtempSummary.setText("" + t.getMessage());

            }
        };
        forecastService.loadForecastData(forecast);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == Permission.REQUEST_ACCESS_INTERNET) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // TO DO: accept
            }
            else {
                // TO DO: do not accept
            }
            if (requestCode == Permission.REQUEST_ACCESS_LOCATION) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Coordinates c = getGPStrackerLocation();
                    btnGetLocation.setText("" + c.getLat() + "," + c.getLon());
                }
                else {
                    btnGetLocation.setText(("Get Permissions"));
                }
            }

            if (requestCode == Permission.REQUEST_WRITE_STORAGE) {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //btnSaveGPX.setText("Write GPX");
                }
                else {
                    //btnSaveGPX.setText("Get Write SD Permission");
                }
            }
            if (requestCode == Permission.REQUEST_ACCESS_INTERNET_LOCATION) {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    btnGetInfo.setText("Get Info");
                }
                else {
//                    btnGetTemp.setText(("Get Permissions"));
                    btnGetInfo.setText("Get Permissions");
                }
            }
        }
    }
}
