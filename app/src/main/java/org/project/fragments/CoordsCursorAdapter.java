package org.project.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.project.R;
import org.project.Utils;
import org.project.gps.Coordinates;
import org.project.gps.DbCoordinates;

public class CoordsCursorAdapter extends CursorAdapter {
    private Context generalContext;

    public CoordsCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
        this.generalContext = context;
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return  LayoutInflater.from(context).inflate(R.layout.row, parent, false);

    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Find fields to populate in inflated template
        TextView tvCoord = (TextView) view.findViewById(R.id.row_coords);
        TextView tvSms = (TextView) view.findViewById(R.id.row_sms);
        TextView tvMap = (TextView) view.findViewById(R.id.row_map);
        TextView tvShare = (TextView) view.findViewById(R.id.row_share);

        final Coordinates c = DbCoordinates.setCoordsDB(cursor);

        // Extract properties from cursor
        String name = c.getCoordinates();
        // Populate fields with extracted properties
        tvCoord.setText(name);

        tvCoord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCoordClicked(c);
            }
        });

        tvMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(Utils.tag, "Clicked on tvMap");
                onMapClicked(c);
            }
        });
        tvSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSmsClicked(c);
            }
        });

        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShareClicked(c);
            }
        });


    }

    private void onCoordClicked(Coordinates c) {
        ClipboardManager clipboard = (ClipboardManager) generalContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("geo:", c.getCoordinates());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(generalContext,c.getCoordinates() + "\ncopied to clipboard", Toast.LENGTH_SHORT).show();
    }

    private void onShareClicked(Coordinates c) {
        Double latitude = c.getLat();
        Double longitude = c.getLon();

        String uri = "http://maps.google.com/maps?saddr=" +latitude+","+longitude;

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String ShareSub = "Here is my location";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri);
        generalContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    private void onMapClicked(Coordinates c) {
        Uri gmmIntentUri = Uri.parse("geo:0,0?q="+c.getLat()+","+c.getLon() + " (Location of pin)");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        generalContext.startActivity(mapIntent);
    }

    private void onSmsClicked(Coordinates c) {
        final Intent textIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("sms:?body=" + c.getCoordinates()));
        generalContext.startActivity(textIntent);

    }

}
