# Project - aplikacja Android
### Działanie
Aplikacja służy do logowania trasy użytkownika za pomocą GPS, umożliwia eksport trasy za pomocą formatu GPX, wyświetlanie aktualnej pogody, oraz wyświetlenie miejsce znajdowania się użytkownika za pomocą współrzędnych GPS. Aplikacja umożliwia również rejestrację oraz logowanie użytkowników. Po zalogowaniu użytkownik może również sprawdzić swoje dane.
### Wymagania do projektu
1. Prawidłowe obsłużenie cyklu życia, aplikacji
2. Implementacja kilku aktywności ( pliki: AcitivtyLogin, ActivitySignup, MainFragment)
3. Wykorzystanie Shared Preferences ( funkcja getSharedPrefs w AcitvitySignup)
4. Zmiana orientacji urządzenia (folder layout)
5. Aplikacja dla różnych rozdzielczości (folder layout)
6. Wykorzystanie fragmentów ( pliki: Fragment2, GPSLocation, UserInfo, SectionsStatePagerAdapter, MainFragment)
7. Pliki, z pamięci zewnętrznej, karta SD (plik Storage)
8. Dostęp do bazy danych (sqlite) - (pliki: DbCoordinates oraz DbUser)
9. Dostęp do serwisów web, za pomocą technologii REST (pliki Forecast, ForecastAPI, ForecastService)
10. Wykorzystanie formatu XML do kodowania danych (plik: GPX)
11. Uruchomienie serwisów w Androidzie ( plik GPSService)
12. Wykorzystanie notyfikacji ( plik GPSService)
13. Wykorzystanie filtrów ( pliki GPSLocation, GPSService)
15. Wykorzystanie zewnętrzynch urządzeń, GPS (pliki GPSService, GPSLocation)
16. Odtwarzanie multimediów, muzyki ( funkcja runJingle w AcitivtyLogin)
17. Wykorzystanie SMSów ( funkcja onSmsClicked w CoordsCursorAdapter)
19. Obsługa praw dostępu do sieci, do plików oraz do GPS - (plik Permission)
20. Wykorzystanie różnych layoutów - 
    a,b,c,f - activiy_signup.xml
    d - main_fragment.xml
    e - frag_userinfo_layout.xml
21. Implementacja listenerów - Broadcast receiver w pliku GPSLocation, mnóstwo listenerów pod przyciskami w plikach GPSLocation, ActivitySignup, we fragmentach itd.)
22. Wykorzystanie DataAdaptera: ListView, korzystające z CoursorAdaptera, pliki CoordsCursorAdapter, GPSLocation, frag_gpslocation_layout.xml)
23. Widżety:
    a,b,c,e, f, g, h - activity_signup.xml wykorzystany w AcitivitySignup
    d - black_logo.xml, white_logo.xml, logo.png, negate_logo.png, ikonki apliakacji itd.
   j - plik row.xml wykorzystany we fragmencie GPSLocation

### Poza tym:
* Wykorzystałem dwa różne sposoby korzystania z GPS, jako serwis, plik GPSService, oraz pobieranie ostatnio znanej lokacji - plik GPStracker
* Stworzenie własnych przycisków, gradientów w aplikacji, pliki: btn_bg.xml, grad_bg.xml, reverse_grad_bg.xml, własne logo aplikacji
* Animacja podczas włączania się aplikacji, kiedy nie zalogowany, activity_login.xml oraz ActivityLogin
* Wykorzystanie SeekBar, oraz podpięcie do niego listenerów pliki alert_dialog_gps.xml oraz funkcja setGpsSettings w pliku GPSLocation)
* Wykorzystanie ustandaryzowanego formatu zapisu koordynat GPS do pliku .xml w formacie GPX, co umożliwia eksportowanie trasy do innych aplikacjii
* Wykorzystanie AlertDialog - (funkcja setGpsSettings w pliku GPSLocation)
* Wykorzystanie DatePickerDialog - funkcja showDatePickerDialog w pliku ActivitySignup
* Wykorzystanie map Google do wyświetlania lokacji, funkcja onCoordClicked w pliku CoordsCursorAdapter
* Wykorzystanie ClipboardManger'a - funkcja onCoordClicked w CoordsCursorAdapter
* Wykorzystanie Intent.ACTION_SEND - do share'owanie koordynat w innych aplikacjach, funkcja onShareCLicked w pliku CoordsCursorAdapter
* Wykorzystanie Geocodera za pomocą którego możliwe jest uzyskanie adresu na podstawie koordynat GPS, funkcja getAddress w pliku Fragment2.java
* Walidacja wpisanych danych podczas rejestracji oraz sprawdzenie czy użytkownik zalogowany, pliki ActivitySignup, ActivityLogin


#### Screenshoty z aplikacji w folderze screenshots